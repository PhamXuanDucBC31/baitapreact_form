import axios from "axios";

let https = axios.create({
  baseURL: "https://62b07878196a9e9870244798.mockapi.io",
});

export const userService = {
  getListUser: () => {
    let uri = "/react_form";
    return https.get(uri);
  },

  deleteUser: (id) => {
    let uri = `/react_form/${id}`;
    return https.delete(uri);
  },
  postUser: (user) => {
    let uri = "/react_form";
    return https.post(uri, user);
  },
  getDetailUser: (id) => {
    let uri = `/react_form/${id}`;
    return https.get(uri);
  },
  putUserEdited: (user, id) => {
    let uri = `/react_form/${id}`;
    return https.put(uri, user);
  },
};
