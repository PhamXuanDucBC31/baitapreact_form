import React, { Component } from "react";
import { initialUser } from "./ulti_exQuanLyNguoiDung";

export default class FormNguoiDung extends Component {
  state = {
    user: initialUser,
    isUserEdited: false,
  };

  handleChangeUser = (event) => {
    let value = event.target.value;
    let key = event.target.name;
    let cloneUser = { ...this.state.user, [key]: value }; //[key] --- dynamic key
    this.setState({ user: cloneUser });
  };

  static getDerivedStateFromProps(props, state) {
    if (!state.user.hoTen && props.userEdited && !state.isUserEdited) {
      return { user: props.userEdited, isUserEdited: true };
    }
  }
  render() {
    return (
      <div>
        <form>
          <div className="form-group">
            <input
              onChange={(e) => {
                this.handleChangeUser(e);
              }}
              value={this.state.user.hoTen}
              type="text"
              className="form-control"
              name="hoTen"
              placeholder="Tên"
            />
          </div>
          <div className="form-group">
            <input
              onChange={(e) => {
                this.handleChangeUser(e);
              }}
              value={this.state.user.taiKhoan}
              type="text"
              className="form-control"
              name="taiKhoan"
              placeholder="Tài Khoản"
            />
          </div>
          <div className="form-group">
            <input
              onChange={(e) => {
                this.handleChangeUser(e);
              }}
              value={this.state.user.matKhau}
              type="text"
              className="form-control"
              name="matKhau"
              placeholder="Mật khẩu"
            />
          </div>
          <div className="form-group">
            <input
              onChange={(e) => {
                this.handleChangeUser(e);
              }}
              value={this.state.user.email}
              type="text"
              className="form-control"
              name="email"
              placeholder="Email"
            />
          </div>

          {this.props.userEdited ? (
            <button
              onClick={() => {
                this.setState({
                  user: initialUser,
                  isUserEdited: false,
                });
                this.props.handleUpdateUser(this.state.user);
              }}
              className="btn btn-success"
              type="button"
            >
              Cập nhật người dùng
            </button>
          ) : (
            <button
              type="button"
              onClick={() => {
                this.setState({
                  user: initialUser,
                });
                this.props.handleADD(this.state.user);
              }}
              className="btn btn-primary"
            >
              Thêm người dùng
            </button>
          )}
        </form>
      </div>
    );
  }
}
