import React, { Component } from "react";
import axios from "axios";
import shortid from "shortid";
import DanhSachNguoiDung from "./DanhSachNguoiDung";
import FormNguoiDung from "./FormNguoiDung";
import { initialUser } from "./ulti_exQuanLyNguoiDung";
import { userService } from "./user.service";

export default class Ex_QuanLyNguoiDung extends Component {
  fetchDanhSachNguoiDung() {
    userService
      .getListUser()
      .then((res) => {
        this.setState({
          danhSachNguoiDung: res.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  componentDidMount() {
    this.fetchDanhSachNguoiDung();
    // async function fetchData() {
    //   let result = await userService.getListUser();

    //   this.setState({ danhSachNguoiDung: result.data });
    // }
    // fetchData();
  }

  state = {
    danhSachNguoiDung: [],
    userEdited: null,
  };

  handleThemNguoiDung = (user) => {
    let newUser = { ...user, id: shortid.generate() };
    // this.setState({
    //   danhSachNguoiDung: [...this.state.danhSachNguoiDung, newUser],
    // });
    userService
      .postUser(newUser)
      .then((res) => {
        this.fetchDanhSachNguoiDung();
      })
      .catch((err) => {});
  };

  handleRemoveUser = (id) => {
    // let cloneDanhSachNguoiDung = [...this.state.danhSachNguoiDung];

    // let index = this.state.danhSachNguoiDung.findIndex((user) => {
    //   return user.id == id;
    // });

    // if (index !== -1) {
    //   cloneDanhSachNguoiDung.splice(index, 1);
    //   this.setState({
    //     danhSachNguoiDung: cloneDanhSachNguoiDung,
    //   });
    // }

    userService
      .deleteUser(id)
      .then((res) => {
        this.fetchDanhSachNguoiDung();
      })
      .catch((err) => {});
  };

  handleEditUser = (id) => {
    // let editedUser = [...this.state.danhSachNguoiDung];
    // let index = editedUser.findIndex((item) => {
    //   return item.id == id;
    // });
    // if (index !== -1) {
    //   this.setState({
    //     userEdited: editedUser[index],
    //   });
    // }
    userService
      .getDetailUser(id)
      .then((res) => {
        this.setState({
          userEdited: res.data,
        });
      })
      .catch((err) => {});
  };

  handleUpdateUser = (userEdited) => {
    // let index = this.state.danhSachNguoiDung.findIndex((item) => {
    //   return item.id == userEdited.id;
    // });

    // if (index !== -1) {
    //   let cloneDanhSachNguoiDung = [...this.state.danhSachNguoiDung];
    //   cloneDanhSachNguoiDung[index] = userEdited;
    //   this.setState({
    //     danhSachNguoiDung: cloneDanhSachNguoiDung,
    //     userEdited: null,
    //   });
    // }
    userService
      .putUserEdited(userEdited, userEdited.id)
      .then((res) => {
        this.fetchDanhSachNguoiDung();
      })
      .catch((err) => {});
  };

  render() {
    return (
      <div className="container py-5">
        <FormNguoiDung
          handleUpdateUser={this.handleUpdateUser}
          userEdited={this.state.userEdited}
          handleADD={this.handleThemNguoiDung}
        />
        <DanhSachNguoiDung
          handleEditUser={this.handleEditUser}
          handleDelete={this.handleRemoveUser}
          danhSachNguoiDung={this.state.danhSachNguoiDung}
        />
      </div>
    );
  }
}
