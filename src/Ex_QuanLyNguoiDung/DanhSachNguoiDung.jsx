import React, { Component } from "react";

export default class DanhSachNguoiDung extends Component {
  renderDanhSachNguoiDung = () => {
    return this.props.danhSachNguoiDung.map((item, index) => {
      let { hoTen, taiKhoan, matKhau, email, id } = item;
      return (
        <tr key={index}>
          <td>{id}</td>
          <td>{hoTen}</td>
          <td>{taiKhoan}</td>
          <td>{email}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleDelete(id);
              }}
              className="btn btn-danger mr-1"
            >
              Xoá
            </button>
            {/* -------------------- */}
            <button
              onClick={() => {
                this.props.handleEditUser(id);
              }}
              className="btn btn-success ml-1"
            >
              Sửa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table mt-2">
          <thead>
            <td>ID</td>
            <td>Tên</td>
            <td>Tài khoản</td>
            <td>Email</td>
            <td>Thao tác</td>
          </thead>

          <tbody>{this.renderDanhSachNguoiDung()}</tbody>
        </table>
      </div>
    );
  }
}
